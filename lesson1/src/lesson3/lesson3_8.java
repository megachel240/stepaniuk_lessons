package lesson3;

import java.rmi.server.ExportException;

public class lesson3_8 {
    public static void main(String[] args) {
        double max = 3;
        double point = Math.exp(max);
        for (double x = -10; x < 10; x += 0.01)
            if (Math.exp(max) > Math.exp(max)) {
                max = x;
                point = Math.exp(max);
            }
        System.out.println(max);
        System.out.println(point);
    }
}
