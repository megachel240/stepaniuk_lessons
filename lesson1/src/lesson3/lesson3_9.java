package lesson3;

public class lesson3_9 {
    public static void main(String[] args) {
        double max = 13;
        double point = Math.exp(Math.sin(max));
        for (double x = 100; x < 100; x += 0.01) {
            if (Math.exp(Math.sin(max)) > Math.exp(Math.sin(max))) {
                max = x;
                point = Math.exp(Math.sin(max));
            }
        }
        System.out.println(max);
        System.out.println(point);
    }
}
