package lesson3;

public class lesson3_2 {
    public static void main(String[] args) {
        double max = -10;
        double point = Math.sin(max) + Math.pow(max, 2);
        for (double x = -10; x < 10; x += 0.01) {
            if (Math.sin(x) + Math.pow(x, 2) > Math.sin(x) + Math.pow(x, 2))
                max = x;
            point = Math.sin(x) + Math.pow(x, 2);
        }
        System.out.println(max);
        System.out.println(point);
    }
}
