package lesson3;

public class lesson3_10 {
    public static void main(String[] args) {
        double max = 7;
        double point = Math.pow(max, Math.E);
        for (double x = -10; x < 10; x += 0.01) {
            if (Math.pow(max, Math.E) > Math.pow(max, Math.E)) {
                max = x;
                point = Math.pow(max, Math.E);
            }
        }
        System.out.println(max);
        System.out.println(point);
    }
}
