package hometask;

import java.util.Scanner;

public class dz8 {
    public static void main(String[] args) {
        int a = 21;  // входные данные

        int b = 0;   // количество делений без остатка
        for (int i = 1; i <= a; i++) {
            if (a % i == 0)
                b++;
        }
        if (b == 2) {
            System.out.println("Число " + a + " простое");
        } else {
            System.out.println("Число " + a + " не простое");
        }
    }
}


