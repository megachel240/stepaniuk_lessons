package lesson7;

import java.util.Random;

public class lesson7_ht9 {
    public static void main(String[] args) {  //)Проверить, что все элементы в массиве расположены по возрастанию.
        int[] data = new int[6];
        Random random = new Random();
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(5)+10;
            System.out.print(" " + data[i]);
        }
        System.out.println();
        System.out.println(equal(data));
    }

    public static boolean equal(int data[]) {
        for (int i = 0; i < data.length - 1; i++) {
            if (data[i] > data[i + 1]) {
              return   false;
            }
        }
        return true;
    }
}

