package lesson7;

import java.util.Arrays;

public class lesson7_ht10 {
    public static void main(String[] args) {
        //Найти три самых больших элемента в массиве. Например, для массива 5,1,4,8,3,5,2,7] ответ будет 8, 7, 5.
        int data[] = {5, 1, 4, 8, 3, 5, 2, 7};


        for (int j = 0; j < data.length; j++) {
            for (int i = 0; i < data.length - 1; i++) {
                if (data[i] < data[i + 1]) {
                    int tmp = data[i];
                    data[i] = data[i + 1];
                    data[i + 1] = tmp;
                }
            }
        }
        System.out.println("max" + data[0] + " " + data[1] + " " + data[2]);
    }
}

