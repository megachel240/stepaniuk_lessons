package lesson5;

import javax.xml.bind.SchemaOutputResolver;
import java.util.Random;

public class lesson5dz1sort {                                       //Отсортировать столбцы в двумерном массиве по возрастанию суммы элементов в них
    public static void main(String[] args) {
        int[][] data = new int[4][4];
        fillrandom(data, 10);
        printarray(data);
        sortarray(data);
        System.out.println();
        printarray(data);
    }

    static void fillrandom(int[][] data, int a) {
        Random random = new Random();
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                data[i][j] = random.nextInt(a) + 10;
            }
        }
    }

    static void printarray(int data[][]) {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                System.out.print(" " + data[i][j]);
            }
            System.out.println(" "+ summ(data[i]));
        }
    }

    static int summ(int[] data) {
        int summ = 0;
        for (int i = 0; i < data.length; i++) {
            summ += data[i];
        }
        return summ;
    }

    static void sortarray(int[][] data) {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length - 1; j++) {
                if (summ(data[j]) < summ(data[j + 1])) {
                    int[] tmp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = tmp;
                }
            }

        }
    }
}



