package lesson5;

import sun.awt.RepaintArea;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class lesson5_dz2 {                              //)Отсортировать по убыванию двумерный массив
    public static void main(String[] args) {            //)Отсортировать столбцы в двумерном массиве по убыванию суммы элементов в
        Random random = new Random();
        int[][] data = new int[3][3];
        fillrandom(data, 10);
        printarray(data);
        sort2d(data);
        System.out.println();
        printarray(data);
    }

    static void fillarray(int[][] data) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                System.out.println("please enter " + i + " ," + j + "'est element");
                data[i][j] = scanner.nextInt();
            }
        }
    }

    static void printarray(int data[][]) {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                System.out.print(" " + data[i][j]);
            }
            System.out.println(" СУММА СТРОКИ   " + summ(data[i]));
        }
    }

    static int summ(int[] data) {
        int summ = 0;
        for (int i = 0; i < data.length; i++) {
            summ += data[i];
        }
        return summ;
    }

    static void sortarray(int[][] data) {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length - 1; j++) {
                if (summ(data[j]) < summ(data[j + 1])) {
                    int[] tmp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = tmp;
                }
            }

        }
    }


    static void sort2d(int[][] array) {
        int[] tmp = new int[array.length * array[0].length];
        int countofTMP = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                tmp[countofTMP] = array[i][j];
                countofTMP++;
            }
        }
        for (int j = 0; (j < tmp.length); j++) {
            for (int k = 0; k < tmp.length - 1; k++) {
                if (tmp[k] > tmp[k + 1]) {
                    int tmp1 = tmp[k];
                    tmp[k] = tmp[k + 1];
                    tmp[k + 1] = tmp1;
                }
            }

        }

        countofTMP = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = tmp[countofTMP];
                countofTMP++;
            }
        }
    }

    static void fillrandom(int[][] data, int a) {
        Random random = new Random();
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                data[i][j] = random.nextInt(a) + 10;
            }
        }
    }

    static void sortline(int data[]) {
        int summline = 0;
        for (int i = 0; i < data.length; i++) {
            summline += data[i];
            if (data[i] > data[i + 1]) {
                int change = data[i];
                data[i] = data[i + 1];
                data[i + 1] = change;
            }
        }
    }
}
