package lesson5;

import java.util.Random;

public class lessson5dz4 {
    public static void main(String[] args) {
        int data[][] = new int[4][4];
        fillrandom(data);
        printarray(data);
        changeColums(data, 3, 1);
       // System.out.println(data);
        printarray(data);
    }

    static void fillrandom(int[][] data) {
        Random random = new Random();
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                data[i][j] = random.nextInt(10);
            }

        }
    }

    static void changeColums(int[][] array, int x1, int x2)     {
        int tmp = 0;
        for (int i = 0; i < array.length; i++) {
            tmp = array[i][x1];
            array[i][x1] = array[i][x2];
            array[i][x2] = tmp;
        }
    }

    static void printarray(int data[][]) {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                System.out.print(" " + data[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

    static int summElementsColum (int [][] array ,int num){
        int summ=0;
        for (int i = 0; i <array.length ; i++) {
            summ += array[i][num];
        }
        return summ;
    }


    static void sort3d (int[][] array){
        for (int i = 0; i <array[0].length ; i++) {
            for (int j = 0; j <array[0].length -1 ; j++) {
                if(summElementsColum(array,j)<summElementsColum(array,j+1))
                changeColums(array,j,j+1);
            }
        }
    }

}