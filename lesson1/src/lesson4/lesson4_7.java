package lesson4;

import java.util.Random;

public class lesson4_7 {
    public static void main(String[] args) {         //Найти сумму элементов одномерного массива с нечетными номерами
        Random random = new Random();
        double[] data = new double[5];

        double total = 0;
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(5);     //сдвинуть на +22
            System.out.println(data[i]);

            if (data[i] % 2 == 0)                   //тоже самая задача с нечетными только 2!=0
                total += data[i];
        }
            System.out.println("summa" + total);
    }
}