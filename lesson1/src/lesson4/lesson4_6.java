package lesson4;

import java.util.Random;

public class lesson4_6 {
    public static void main(String[] args) {              //5) Найти произведение элементов одномерного массива
        Random random = new Random();
        double[] data = new double[5];
        double total = 1;
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(5) +22;     //сдвинуть на +22
            System.out.println(data[i]);
        }

        for (int i = 0; i < data.length; i++) {
            total *= data[i];
        }
        System.out.println(total);
    }
}