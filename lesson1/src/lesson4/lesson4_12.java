package lesson4;

import java.util.Random;

public class lesson4_12 {
    public static void main(String[] args) {         //Найти количество нечетных элементов в одномерном массиве
        Random random = new Random();
        double[] data = new double[10];
        double total = 1;
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(10);
            System.out.println(data[i]);
        }
        for (int i = 0; i < data.length; i++) {
            if (data[i] % 2 == 0)
                total++;
        }
            System.out.println("количество нечетных элементов " + total);
    }
}

