package lesson4;

import java.util.Random;

public class lesson4_13 {
    public static void main(String[] args) {       // Найдите количество элементов, превышающих заданное число k
        Random random = new Random();
        double total = 0;
        double k = 4;
        double[] data = new double[10];
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(10);
            System.out.println(data[i]);

            if (data[i] > k)
                total++;
        }
        System.out.println("элементов, превышающих заданное число k: " + total);
    }
}