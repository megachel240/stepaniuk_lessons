package lesson4;

import java.util.Random;

public class lesson4_24 {
    public static void main(String[] args) {                // Найти определитель квадратного двумерного массива
        Random random = new Random();
        int[][] data = new int[3][3];
        int summ = 0;
        int a = data.length;
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                data[i][j] = random.nextInt(20) + 10;
                System.out.print(" " + data[i][j]);
            }
            System.out.println();
        }
        summ = data[0][0]*data[1][1]*data[2][2]+data[0][1]*data[1][2]*data[2][1]+data[0][2]*data[1][0]*data[2][1]-data[0][2]*data[1][1]*data[2][0]-data[0][0]*data[1][2]*data[2][1]-data[0][1]*data[1][0]*data[2][2];
        System.out.println(summ);
    }

}
