package lesson4;

public class lesson4_15_2 {
        public static void main(String[] args) {
            double[] myArray = {11, 5.8, 11.1, 10.9};
            double min = myArray[0];
            for (int i = 1; i < myArray.length; i++) {
                if (myArray[i] < min) {
                    min = myArray[i];
                }
            }
            System.out.println("Минимальное значение в массиве myArray: " + min);
    }
}
