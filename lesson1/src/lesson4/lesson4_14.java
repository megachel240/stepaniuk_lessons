package lesson4;

import java.util.Random;

public class lesson4_14 {
    public static void main(String[] args) {                    // количество элементов, не превышающих заданное число k,
        Random random = new Random();
        int total = 0;
        double k = 4;
        double[] data = new double[10];
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(10);
            System.out.println(data[i]);
            if (data[i] < k)
                total++;
        }
        System.out.println(" количество элементов, не превышающих заданное число k:" + total);
    }
}
