package lesson4;

import java.util.Random;

public class lesson4_16 {
    public static void main(String[] args) {
        Random random = new Random();                //через него рандосят цифры
        double[] data = new double[50];
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(99) + 1;           //заполнение массива случайными числами до 99
        }
        //нахождение максимума
        double max = data[0];                            //переменная для хранения max
        for (int i = 0; i < data.length; i++) {
            if (data[i] > max) {
                max = data[i];
            }
            //нормирование (деление на максимальный элемент)
            for (int i1 = 0; i1 < data.length; i1++) {
                data[i1] = data[i1] / max;
            }
            System.out.println("нормирование" + data[i]);
        }
        System.out.println("максимум " + max);

        //печать массива на экран
        for (int i = 0; i < data.length; i++) {
            System.out.println(data[i]);
        }
    }
}